USE [Учебные заведения]
GO
/****** Object:  Table [dbo].[Города]    Script Date: 09/21/2015 00:36:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Города](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
 CONSTRAINT [PK_Города] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Университеты]    Script Date: 09/21/2015 00:36:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Университеты](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
	[id_Города] [int] NULL,
 CONSTRAINT [PK_Университеты] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Факультеты]    Script Date: 09/21/2015 00:36:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Факультеты](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Название] [nchar](100) NULL,
	[id_Университета] [int] NULL,
 CONSTRAINT [PK_Факультеты] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Университеты_Города]    Script Date: 09/21/2015 00:36:07 ******/
ALTER TABLE [dbo].[Университеты]  WITH CHECK ADD  CONSTRAINT [FK_Университеты_Города] FOREIGN KEY([id_Города])
REFERENCES [dbo].[Города] ([id])
GO
ALTER TABLE [dbo].[Университеты] CHECK CONSTRAINT [FK_Университеты_Города]
GO
/****** Object:  ForeignKey [FK_Факультеты_Университеты]    Script Date: 09/21/2015 00:36:07 ******/
ALTER TABLE [dbo].[Факультеты]  WITH CHECK ADD  CONSTRAINT [FK_Факультеты_Университеты] FOREIGN KEY([id_Университета])
REFERENCES [dbo].[Университеты] ([id])
GO
ALTER TABLE [dbo].[Факультеты] CHECK CONSTRAINT [FK_Факультеты_Университеты]
GO
